/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a)
{
    setSize (500, 400);
    
    addAndMakeVisible(ctrlButton);
    ctrlButton.addListener(this);
    
    counter.setListener(this);

}

MainComponent::~MainComponent()
{
}

void MainComponent::resized()
{
    ctrlButton.setBounds(getWidth()/2, getHeight()/2, getWidth()/6, getHeight()/6);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}





void MainComponent::buttonClicked(Button* button)
{
    
    counter.start();
    
    

}

void MainComponent::counterChanged (const unsigned int counterValue)
{
    timeOffset = counterValue;
    std::cout << "Counter: " << (Time::getApproximateMillisecondCounter() - timeOffset) << "\n";

}