//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 07/11/2016.
//
//

#include "Counter.hpp"

Counter::Counter() : Thread("counter")
{

    startThread();
    listener = nullptr;
}


Counter::~Counter()
{
    stopThread(500);
    
}



void Counter::run()
{
    timeOffset = Time::getApproximateMillisecondCounter();
    

    while (!threadShouldExit())
    {
        
        
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
            listener->counterChanged (timeOffset ++);
        Time::waitForMillisecondCounter(time + 101);
    }
}

void Counter::start()
{
    if(isThreadRunning())
    {
        stopThread(500);
    }
    else
    {
        startThread();
    }
    
}

void Counter::setListener (Listener* newListener)
{
    listener = newListener;
}
