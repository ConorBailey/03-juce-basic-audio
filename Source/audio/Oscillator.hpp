//
//  Oscillator.hpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 03/11/2016.
//
//

#ifndef Oscillator_hpp
#define Oscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"



class Oscillator
{
public:
    Oscillator();
    ~Oscillator();
    
    float getSample();
    void setFrequency(float newFrequency);
    void setAmp(float newAmp);
    void setSampleRate(int newSampleRate);
    
private:
    
    Atomic<float> gain;
    Atomic<float> frequency;
    
    float phasePosition;
    int sampleRate;
    
};
#endif /* Oscillator_hpp */