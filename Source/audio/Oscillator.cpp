//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 03/11/2016.
//
//

#include "Oscillator.hpp"

Oscillator::Oscillator()
{
    sampleRate = 44100;
    
    
}
Oscillator::~Oscillator()
{
    
}

float Oscillator::getSample()
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency.get()) / sampleRate;
    
    
    phasePosition += phaseIncrement;
    
    if (phasePosition > twoPi){
        phasePosition -= twoPi;
    }
    
    float out = sin(phasePosition) * gain.get();

    return out;
}

void Oscillator::setFrequency(float newFrequency)
{
    frequency = newFrequency;
    

    
}
void Oscillator::setAmp(float newAmp)
{
    gain = newAmp;
}

void Oscillator::setSampleRate(int newSampleRate)
{
    sampleRate = newSampleRate;
}
